import functools
import traceback

import sys
# for gui tools:

from PySide import QtUiTools
from PySide import QtCore
from PySide.QtGui import QGridLayout


from PySide.QtCore import QFile

import PySide
from PySide import QtGui
import pyqtgraph as pg

import numpy as np
from numpy import arange, sin, cos, pi

# aditional tools:
from functools import partial  # para facilitar el manejo de eventos


class View(QtCore.QObject):  # hereda de la clase QtGui.QmainWindow

    def __init__(self, ui_file_path,parent = None):

        # ------------basic configuration:------------------

        super(View, self).__init__(parent)  # initialize the parent class (QtGui)
        #self.controller = controller

        # load ui file:
        loader = QtUiTools.QUiLoader()
        my_file = QFile(ui_file_path)
        my_file.open(QFile.ReadOnly)
        self.window = loader.load(my_file)
        my_file.close()


        #self.line = self.window.findChild(QLineEdit, 'lineEdit')



        # graphics
        voltage_plot = Plot2D(self.window.voltageGraph, title="voltaje")
        current_plot = Plot2D(self.window.currentGraph, title="Corriente")
        voltage_plot.start()
        current_plot.start()

        # sustainability

        #------------configuring configration------


        self.window.show()
        print("view init done!")


class Plot2D(pg.GraphicsWindow):
    def __init__(self, widget, title = "graphic"):
        # Enable antialiasing for prettier plots
        pg.setConfigOptions(antialias=True)

        #pg.setConfigOption('background', 'w')
        #pg.setConfigOption('foreground', 'k')

        # for the inheritance specifications:
        super(Plot2D, self).__init__(parent=widget)

        # for custom class managment:
        self.t = np.arange(0, 10, 0.05)
        self.s = []
        self.c = []
        self.i = 0  # this is the variable which contain the steps done by the time axis
        self.traces = dict()
        self.timer = pg.QtCore.QTimer()
        self.timer.timeout.connect(self.update_data)

        # to add this widget to the widget parent:
        layout = pg.QtGui.QVBoxLayout()
        widget.setLayout(layout)
        # self.setParent(widget)
        layout.addWidget(self)

        #self.plot_space.setWindowTitle('pyqtgraph example: Scrolling Plots')

        self.plot1 = self.addPlot(title=title)
        self.plot1.setLabels(left='Valor')
        self.plot1.setLabels(bottom='tiempo')

        ## set pen on a single data set:
        #plotline.setPen(color=(255, 0, 0), width=3)



    def start(self):
        self.timer.start(40) # this is in miliseconds 24 frames per second

    def trace(self, name, dataset_x, dataset_y):
        if name in self.traces:
            self.traces[name].setData(dataset_x, dataset_y)
            #self.traces[name].setPos(dataset_x, 0)
        else:
            if name == 'sin':
                self.traces[name] = self.plot1.plot(pen={'color': (255, 0, 0), 'width': 3})
                # self.waveform.setYRange(-32768, 32767, padding=0)
                # self.waveform.setXRange(0, self.CHUNK, padding=0.005)
            if name == 'cos':
                self.traces[name] = self.plot1.plot(pen={'color': (0, 255, 0), 'width': 3})


    def update_data(self):

        #create the new y vector list:
        # for compatibility reasons we convert them to np arrays (also works with lists)
        self.t += 0.040
        self.s =  np.array(sin(2 * pi * 0.5 * self.t))
        self.c =  np.array(cos(2 * pi * 0.2 * self.t))

        #Graph dem epending on de function:
        self.trace("sin", self.t, self.s)
        self.trace("cos", self.t, self.c)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    ## Always start by initializing Qt (only once per application)
    app = QtGui.QApplication([])

    ## Define a top-level widget to hold everything
    w = QtGui.QWidget()
    p = Plot2D(w)
    p.start()

    # Display the widget as a new window
    w.show()

    sys.exit(app.exec_())

