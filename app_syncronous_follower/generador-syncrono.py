
from controllers import *
from models import *

import sys
from PySide import QtGui

#custom qt widgets (chek box):    http://doc.qt.io/archives/qt-4.8/stylesheet-examples.html
#                                 https://doc.qt.io/archives/qq/qq20-qss.html
#                                 https://stackoverflow.com/questions/40672817/how-to-make-color-for-checked-qradiobutton-but-looks-like-standard
#                                 http://doc.qt.io/qt-5/stylesheet-examples.html


def main():

    """"
    # this was used to solve one problem with installation, no needed for the main program
    reload(sys)
    sys.setdefaultencoding('utf8')
    """
    app = QtGui.QApplication(sys.argv)

    my_controller = Controller()
    my_view = View("./generador-syncrono.ui")
    my_model = Model(my_controller)


    sys.exit(app.exec_())

class App:

    def __init__(self, controller):
        pass

if __name__ == "__main__":
    main()


