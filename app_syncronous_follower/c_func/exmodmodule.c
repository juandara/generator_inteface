
// to compile C in windows use the comand:c> gcc program.c -o outputName.exe
// to excecute the program use:> start outputName.exe  (or):> ./outputName.exe 

#include <Python.h>

//#include "C:\Python34\include\Python.h"

static PyObject *exmodError;

static PyObject* say_hello(PyObject* self, PyObject *args)
{
    const char *msg;
    int sts=0;

    //we espect at least one argument in the args so we check it:
    if (!PyArg_ParseTuple(args, "s", &msg)){
        return NULL;    //return error if there is no py argument given
    }

    //then we check if there is some problem with the information given in the argument
    if(stricmp(msg, "this_is_an_error") == 0){
        //we want to raise an error for some reason (in this case the text is "this_is...")
        PyErr_SetString(exmodError, "this is an exeption, something went wrong with the argument");
        return NULL; //propagate the error to the python interpreter
    }else{
        //no error found continue with the normal procidure of the func
        printf("from c world your message was: \n%s", msg);
        sts=21; // return 0 for success
    }

    return Py_BuildValue("i", sts);
}

int main(){
	return 0;
}
