from subprocess import check_output
from subprocess import call
from subprocess import Popen, PIPE

import time

while(True):

    args = "1 34 50"
    comand = "func1"

    t1 = time.time()
    # retorna bytes de respuesta, dont allow program to return duferent than 0
    #result = check_output(comand+" "+args, shell=True)
    t2 = time.time()

    print("-----el resultado de la func 1 es:---------")
    #print(result.decode("utf-8", "replace"))  # 'cp437' is the decoding tipe for windows:
    print("-----fin func 1:---------")

    t2 = time.time()
    # this retun gives the int return program allows to return diferent than cero
    # alse prints in terminal de results bat they cannot be procesed
    #result = call(["func1", "12", "32", "12"])
    t3 = time.time()

    print("-----el resultado de la func 2 es:---------")  # 'cp437' is the decoding tipe for windows
    #print(result)
    print("-----fin func 2:---------")

    t3 = time.time()
    p1 = Popen(["func1", "12", "32", "12"], stdout=PIPE)
    t4 = time.time()

    print("-----el resultado de la func 3 es:---------")
    print(p1.communicate())
    print("-----fin func 3:---------")

    print("tiempo forma 1: "+str(t2 - t1))
    print("tiempo forma 2: "+str(t3 - t2))
    print("tiempo forma 3: "+str(t4 - t3))

