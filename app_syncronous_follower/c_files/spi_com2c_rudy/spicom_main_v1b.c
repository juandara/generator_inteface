/*
 * spicom_main_v1b.c
 *
 *  Created on: Nov 15, 2015
 *      Author: restrepo
 */

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "dsp_spi.h"
//------------------------------------
#define		M_2PI		6.2831853071795864769
#define		M_2PI_3		2.0943951023931954923
//---------------------------------
#define		LEN				10
#define		BASE_ADDR		0x8000000
//-------------------------------------------
#define PRU_NUM 0


const char *device = "/dev/spidev0.1";
const char *filename = "/home/pi/C_exe/file.dat";
uint8_t mode = SPI_CPOL|SPI_CPHA;
uint8_t bits = 8;
uint32_t speed = 1000000;
uint16_t delay = 0;
//fd[0]=SPI; fd[1]=gpio22; fd[2]=gpio18; fd[3]=gpio23; fd[4]=gpio[24]
int fd[5];
int iter=0;
float flag=0;

//static void *pru0DataMemory;
//static unsigned int *pru0DataMemory_int;

//void write_dsp(int fd, int addr, int *buffer, int len);
void write_dsp(int fd, int *data, int addr, int len);

void pabort(const char *s)
{
	perror(s);
	abort();
}

int main(void)
{
 //pthread_t thread;

 //-------------------
 int ret;
 unsigned int it;
 FILE *Fdata;
 float data_in[LEN]={0,};
 float wbuff[LEN]={0,};


 fd[0]=open(device,O_RDWR);
 if (fd[0] < 0)
 		pabort("can't open SPI");

 /*******************************************************
 * spi mode
 *******************************************************/
 ret = ioctl(fd[0], SPI_IOC_WR_MODE, &mode);
 if (ret == -1)
	 pabort("can't set spi mode");
 ret = ioctl(fd[0], SPI_IOC_RD_MODE, &mode);
 if (ret == -1)
	 pabort("can't get spi mode");

 /*******************************************************
 *  bits per word
 ******************************************************/

 ret = ioctl(fd[0], SPI_IOC_WR_BITS_PER_WORD, &bits);
 if (ret == -1)
	 pabort("can't set bits per word");

 ret = ioctl(fd[0], SPI_IOC_RD_BITS_PER_WORD, &bits);
 if (ret == -1)
	 pabort("can't get bits per word");

 /*******************************************************
 *  max speed hz
 ******************************************************/

 ret = ioctl(fd[0], SPI_IOC_WR_MAX_SPEED_HZ, &speed);
 if (ret == -1)
	 pabort("can't set max speed hz");

 ret = ioctl(fd[0], SPI_IOC_RD_MAX_SPEED_HZ, &speed);
 if (ret == -1)
	 pabort("can't get max speed hz");

 //void write_dsp(int fd, int *data, int addr, int len)
 //void read_dsp(int fd,float *data,int addr,int len);
 read_dsp(fd[0],(float *)data_in,BASE_ADDR,LEN);

 for(it=0;it<LEN;it++)
	 wbuff[it]=it;
 read_dsp(fd[0],(float *)data_in,BASE_ADDR,LEN);
 for(it=0;it<LEN;it++)
 	 wbuff[it]=it;
 // I am writing 2 points only
 write_dsp(fd[0],(int*)wbuff,BASE_ADDR,2);

 if ((Fdata= fopen(filename, "w+")) == NULL)
 {
     fprintf(stderr, "Cannot open file. Try again later.\n");
     exit(1);
 }
 for(iter=0;iter<LEN;iter++)
 {
	 fprintf(Fdata,"%f\n",data_in[iter]);
 }
 close(fd[0]);
 fclose(Fdata);
 return(0);
 for(;;)
	 asm("nop;");
}


