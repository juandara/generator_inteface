/*
 * dsp_spi_c.c
 *
 *  Created on: Oct 14, 2016
 *      Author: restrepo
 */

#include <poll.h>
#include <time.h>
#include "dsp_spi.h"

#define	SPIBUFSIZ		1000

//int DATdebug[20];

extern uint8_t bits;
extern uint32_t speed;
extern uint16_t delay;

static inline uint8_t reverse(uint8_t b) {
	b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
	b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
	b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
	return b;
}

// por corregir longitud de escritura
void write_dsp(int fd, int *data, int addr, int len)
{
	int ret,*vptr;
	unsigned int N1,N2,it,i;
	union cmnds dtmp,daddr;
	uint8_t tx1[4] = {0x03,0xEA,0x01,0x05};
	uint8_t rx1[ARRAY_SIZE(tx1)] = {0, };
	uint8_t tx2[4];// = {0x08,0x00,0x00,0x00};
	uint8_t rx2[ARRAY_SIZE(tx2)] = {0, };
	uint8_t tx3[4004] = {0,};
	uint8_t rx3[ARRAY_SIZE(tx3)] = {0, };

	struct spi_ioc_transfer tr[3];

	N1=len/SPIBUFSIZ;
	N2=len%SPIBUFSIZ;

	tr[0].tx_buf= (unsigned long)tx1;
	tr[0].rx_buf= (unsigned long)rx1;
	tr[0].len= ARRAY_SIZE(tx1);
	tr[0].delay_usecs= delay;
	tr[0].speed_hz= speed;
	tr[0].bits_per_word= bits;
	tr[0].cs_change= 0;
	tr[0].pad=0;

	tr[1].tx_buf= (unsigned long)tx2;
	tr[1].rx_buf= (unsigned long)rx2;
	tr[1].len= ARRAY_SIZE(tx2);
	tr[1].delay_usecs= delay;
	tr[1].speed_hz= speed;
	tr[1].bits_per_word= bits;
	tr[1].cs_change= 0;
	tr[1].pad=0;

	tr[2].tx_buf= (unsigned long)tx3;
	tr[2].rx_buf= (unsigned long)rx3;
	tr[2].len= ARRAY_SIZE(tx3);
	tr[2].delay_usecs= delay;
	tr[2].speed_hz= speed;
	tr[2].bits_per_word= bits;
	tr[2].cs_change= 0;
	tr[2].pad=0;

	tx3[0]=0xA5;
	tx3[1]=0x02;
	tx3[2]=0x03;
	tx3[3]=0x04;

	daddr.word=addr;
	vptr=data;

	//////////////////////////////////////////////////////////////////////
	if(N1>0)
	{
		dtmp.nibble.bH=0x0801;
		dtmp.nibble.bL=SPIBUFSIZ;

		tx1[0]=reverse(dtmp.byte.b3);
		tx1[1]=reverse(dtmp.byte.b2);
		tx1[2]=reverse(dtmp.byte.b1);
		tx1[3]=reverse(dtmp.byte.b0);

		for(it=0;it<N1;it++)
		{
			tx2[0]=reverse(daddr.byte.b3);
			tx2[1]=reverse(daddr.byte.b2);
			tx2[2]=reverse(daddr.byte.b1);
			tx2[3]=reverse(daddr.byte.b0);

			for(i=0;i<SPIBUFSIZ;i++)
			{
				dtmp.word=vptr[i];
				tx3[4*i]=reverse(dtmp.byte.b0);
				tx3[4*i+1]=reverse(dtmp.byte.b1);
				tx3[4*i+2]=reverse(dtmp.byte.b2);
				tx3[4*i+3]=reverse(dtmp.byte.b3);
			}
			vptr+=SPIBUFSIZ;
			daddr.word+=SPIBUFSIZ;

			//ret = ioctl(fd, SPI_IOC_MESSAGE(3), tr);
			ret = ioctl(fd, SPI_IOC_MESSAGE(1), tr);
			ret = ioctl(fd, SPI_IOC_MESSAGE(1), tr+1);
			ret = ioctl(fd, SPI_IOC_MESSAGE(1), tr+2);
			if (ret < 1)
				pabort("can't send spi message");
		}
	}
	if(N2>0)
	{
		dtmp.nibble.bH=0x0801;
		dtmp.nibble.bL=N2;

		tx1[0]=reverse(dtmp.byte.b3);
		tx1[1]=reverse(dtmp.byte.b2);
		tx1[2]=reverse(dtmp.byte.b1);
		tx1[3]=reverse(dtmp.byte.b0);

		tx2[0]=reverse(daddr.byte.b3);
		tx2[1]=reverse(daddr.byte.b2);
		tx2[2]=reverse(daddr.byte.b1);
		tx2[3]=reverse(daddr.byte.b0);

		for(i=0;i<N2;i++)
		{
			dtmp.word=vptr[i];
			tx3[4*i]=reverse(dtmp.byte.b0);
			tx3[4*i+1]=reverse(dtmp.byte.b1);
			tx3[4*i+2]=reverse(dtmp.byte.b2);
			tx3[4*i+3]=reverse(dtmp.byte.b3);
		}
		tr[2].len= 4*N2;

		ret = ioctl(fd, SPI_IOC_MESSAGE(1), tr);
		ret = ioctl(fd, SPI_IOC_MESSAGE(1), tr+1);
		ret = ioctl(fd, SPI_IOC_MESSAGE(1), tr+2);
		if (ret < 1)
			pabort("can't send spi message");
	}
	asm("nop;");
}

void read_dsp(int fd_spi,float *data,int addr,int len)
{
	int ret;
	unsigned int it,i;
	volatile unsigned int N1,N2;
	union cmnds dtmp,daddr;
	uint8_t tx1[4] = {0x03,0xEA,0x01,0x05};
	uint8_t rx1[ARRAY_SIZE(tx1)] = {0, };
	uint8_t tx2[4];// = {0x08,0x00,0x00,0x00};
	uint8_t rx2[ARRAY_SIZE(tx2)] = {0, };
	uint8_t tx3[4008] = {0,};
	uint8_t rx3[ARRAY_SIZE(tx3)] = {0, };

	struct spi_ioc_transfer tr[3];

	N1=len/SPIBUFSIZ;
	N2=len%SPIBUFSIZ;

	tr[0].tx_buf= (unsigned long)tx1;
	tr[0].rx_buf= (unsigned long)rx1;
	tr[0].len= ARRAY_SIZE(tx1);
	tr[0].delay_usecs= delay;
	tr[0].speed_hz= speed;
	tr[0].bits_per_word= bits;
	tr[0].cs_change= 0;
	tr[0].pad=0;

	tr[1].tx_buf= (unsigned long)tx2;
	tr[1].rx_buf= (unsigned long)rx2;
	tr[1].len= ARRAY_SIZE(tx2);
	tr[1].delay_usecs= delay;
	tr[1].speed_hz= speed;
	tr[1].bits_per_word= bits;
	tr[1].cs_change= 0;
	tr[1].pad=0;

	tr[2].tx_buf= (unsigned long)tx3;
	tr[2].rx_buf= (unsigned long)rx3;
	tr[2].len= ARRAY_SIZE(tx3);//--------------------
	tr[2].delay_usecs= delay;
	tr[2].speed_hz= speed;
	tr[2].bits_per_word= bits;
	tr[2].cs_change= 0;
	tr[2].pad=0;

	tx3[0]=0xA5;	// revisar esto
	tx3[1]=0x02;
	tx3[2]=0x03;
	tx3[3]=0x04;
	daddr.word=addr;
	it=0;

	/*
	 *  solamente transmitimos máximo SPIBUFSIZ puntos
	 */

	if(N1>0)
	{

		dtmp.nibble.bH=0x0501;
		dtmp.nibble.bL=1001;


		tx1[0]=reverse(dtmp.byte.b3);
		tx1[1]=reverse(dtmp.byte.b2);
		tx1[2]=reverse(dtmp.byte.b1);
		tx1[3]=reverse(dtmp.byte.b0);

		for(it=0;it<N1;it++)
		{
			tx2[0]=reverse(daddr.byte.b3);
			tx2[1]=reverse(daddr.byte.b2);
			tx2[2]=reverse(daddr.byte.b1);
			tx2[3]=reverse(daddr.byte.b0);

			daddr.word+=SPIBUFSIZ;
			//ret = ioctl(fd_spi, SPI_IOC_MESSAGE(3), tr);
			ret = ioctl(fd_spi, SPI_IOC_MESSAGE(1), tr);
			ret = ioctl(fd_spi, SPI_IOC_MESSAGE(1), tr+1);
			ret = ioctl(fd_spi, SPI_IOC_MESSAGE(1), tr+2);
			if (ret < 1)
				pabort("can't send spi message");

			for(i=0;i<SPIBUFSIZ;i++)
			{
				dtmp.byte.b0=reverse(rx3[i*4+3]);
				dtmp.byte.b1=reverse(rx3[i*4+2]);
				dtmp.byte.b2=reverse(rx3[i*4+1]);
				dtmp.byte.b3=reverse(rx3[i*4+0]);
				data[it*SPIBUFSIZ+i]=dtmp.fdata;//(* (volatile float *) dtmp.word); //((float *)vptr)[0];
			}
		}
	}
	if(N2>0)
	{
		if(N2<10)
			tr[2].len = 40;
		else
			tr[2].len = 4*N2;

		dtmp.nibble.bH=0x0501;
		dtmp.nibble.bL=N2;

		tx1[0]=reverse(dtmp.byte.b3);
		tx1[1]=reverse(dtmp.byte.b2);
		tx1[2]=reverse(dtmp.byte.b1);
		tx1[3]=reverse(dtmp.byte.b0);

		tx2[0] = reverse(daddr.byte.b3);
		tx2[1] = reverse(daddr.byte.b2);
		tx2[2] = reverse(daddr.byte.b1);
		tx2[3] = reverse(daddr.byte.b0);
		for(i=0;i<N2;i++)
		{
			dtmp.word=i+1;
			tx3[i*4+3]=reverse(dtmp.byte.b0);
			tx3[i*4+2]=reverse(dtmp.byte.b1);
			tx3[i*4+1]=reverse(dtmp.byte.b2);
			tx3[i*4+0]=reverse(dtmp.byte.b3);
		}
		ret = ioctl(fd_spi, SPI_IOC_MESSAGE(3), tr);
		if (ret < 1)
			pabort("can't send spi message");
		for(i=0;i<N2;i++)
		{
			dtmp.byte.b0=reverse(rx3[i*4+3]);
			dtmp.byte.b1=reverse(rx3[i*4+2]);
			dtmp.byte.b2=reverse(rx3[i*4+1]);
			dtmp.byte.b3=reverse(rx3[i*4+0]);
			data[it*SPIBUFSIZ+i]=dtmp.fdata;//(* (volatile float *) dtmp.word); //((float *)vptr)[0];
			//DATdebug[i]=dtmp.word;
		}
		asm("nop;");
	}

	asm("nop;");
}
