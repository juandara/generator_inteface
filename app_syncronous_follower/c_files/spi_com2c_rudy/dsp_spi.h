/*
 * dsp_spi.h
 *
 *  Created on: Nov 15, 2015
 *      Author: restrepo
 */

#ifndef DSP_SPI_H_
#define DSP_SPI_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#define		SHARC_BASE		0x08000000
#define		FPGA_VER_L		0x0C000003
#define		FPGA_VER_H		0x0C000004
#define		SHARC_START		0x0C000300
#define		SHARC_STOP		0x0C000301
#define		SHARC_DATA1		0x0C000311
#define		SHARC_DATA2		0x0C000312
#define		SHARC_DATA3		0x0C000313
#define		SHARC_DATA4		0x0C000314
#define		SHARC_DATA5		0x0C000315
#define		SHARC_DATA6		0x0C000316

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

//#define	test_wrd1	0x08000000
//#define test_wrd1	0x0C000300
//#define PWM_A1			0x0C000010

union cmnds{
	struct{
		uint8_t b3 :8;
		uint8_t b2 :8;
		uint8_t b1 :8;
		uint8_t b0 :8;
	} byte;
	struct{
		unsigned int bL :16;
		unsigned int bH :16;
	} nibble;
 int word;
 float fdata;
};

void pabort(const char *s);
void write_dsp(int fd, int *data, int addr, int len);
void read_dsp(int fd,float *data,int addr,int len);


#endif /* DSP_SPI_H_ */
