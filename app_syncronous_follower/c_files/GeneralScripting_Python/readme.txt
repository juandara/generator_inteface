This example demonstrates some Python Scripting. It does basic things like configuring the IDDE object, to gain access to the OLE objects (and therefore use the API).

It will connect to a session (session name must exactly match that under 'Session'->'Session List'), open a project (in this case C:\Examples\test.dpj).

It then builds and loads the DXE, sets some breakpoints, fills memory from a binary file, runs the processor, dumps the memory to a binary file.