#Import the win32com.client, so we can access the OLE objects
import win32com.client

#Create a VisualDSP++ ADspApplication object so we can access the API
app = win32com.client.Dispatch( "VisualDSP.ADspApplication" )

"""
If there is no session connected, try to connect to one.
If we fail to connect, there's not much more to be done
"""
if (app.ActiveSession == None):
    app.OutputWindow.PrintText("No session connected. Attempting to connect to session \"BF537Simulator\"", 1)
    app.SessionList.CreateSession("BF537Simulator")
    if (app.ActiveSession == None):
        app.OutputWindow.PrintText("Could not connect to session", 1)
    else:
        session = app.ActiveSession
        app.OutputWindow.PrintText("Connected!", 1)
else:
    session = app.ActiveSession
    app.OutputWindow.PrintText("Session already connected!", 1)

"""
If there is no open project, attempt to open C:\Examples\test.dpj
If this fails, issue an error
"""
if (app.ProjectList.Count == 0):
    app.OutputWindow.PrintText("No Open Project. Attempting to open \"C:\\examples\\test.dpj\"", 1)
    app.ProjectList.AddProject("C:\\examples\\test.dpj")
    activeProject = app.ProjectList.ActiveProject
    if (app.ProjectList.Count == 0):
        app.OutputWindow.PrintText("Could not open project", 1)
    else:
        activeProject = app.ProjectList.ActiveProject
        app.OutputWindow.PrintText("Project loaded", 1)
else:
    activeProject = app.ProjectList.ActiveProject
    app.OutputWindow.PrintText("Project loaded", 1)

"""
Build the project. "True" means the API will wait until the build
completes before continuing
"""
app.OutputWindow.PrintText("Building Project.", 1)
activeProject.BuildAll(True)

"""
Configure our processor object. This will allow us
to load the DXE, set breakpoints, run the processor, etc
"""
processor = session.ProcessorList(0)
app.OutputWindow.PrintText("Loading DXE.", 1)
processor.LoadProgram("C:\\examples\\Debug\\test.dxe")

"""
Set a few breakpoints
"""
app.OutputWindow.PrintText("Setting Breakpoints.", 1)
breakpoint_list=processor.BreakpointList
breakpoint_list.SetBreakpointAtLine(0, "test.c", 8, 0, "", False, True, 0)
breakpoint_list.SetBreakpointAtLine(0, "test.c", 11, 0, "", False, True, 0)
breakpoint_list.SetBreakpointAtLine(0, "test.c", 14, 0, "", False, True, 0)
app.OutputWindow.PrintText("Breakpoints set.", 1)

# Fill the first few address of SDRAM (0x4) from a binary file
app.OutputWindow.PrintText("Fill Memory From Binary File", 1)
memtype = processor.MemoryTypeList.Item(0)
memtype.FillMemoryFromBinaryFile("C:\\Examples\\DataFile.dat", 0x4, 16, 1, 0)

"""
Run the application
"""
app.OutputWindow.PrintText("Running Application.", 1)
processor.Run (True)

# Dump the SDRAM contents memory to binary file
app.OutputWindow.PrintText("Dump Memory To Binary File", 1)
memtype.DumpMemoryToBinaryFile("C:\\Examples\\Dump.dat", 0x4, 16, 1, 0, False)

app.Quit
