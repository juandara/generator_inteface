/*****************************************************************************
 * Regul_VGEN_AC+IsenPhi.c
 *****************************************************************************/
#include<21369.h>
#include<filters.h>
#include<math.h>
#include<sru.h>
#include <stats.h> 
#include"lib_spi_v5b.h"
#include"testrig.h"
//******************************************
//CONSTANTES UTILES
//******************************************
#define Un 	        	169.81
#define In              11.73
#define Wn              377.0 
#define Sn              3000.0
#define Fn              0.45 
#define Wrn             188.5
#define	Tn              15.91 
#define Zn              14.47
#define Ln              0.0383
#define Bn              0.0844
//******************************************
//ASIGNACIONES UTILES
//******************************************
#define LEN    1000
#define LEN1   167
#define LEN2   (LEN/2)
#define	LADQ   (LEN)
#define SQRT2  1.4142135623730950488016887242097
#define str    d_in.bits.b0  //Pulsador de Arranque
#define stp    d_in.bits.b1  //Pulsador de Parada
#define up     d_in.bits.b2  //Pulsador de Subir Referencia
#define dwn    d_in.bits.b3  //Pulsador de Bajar Referencia
#define c_i52  d_in.bits.b4  //Control del I52 y del Factor de Potencia
#define I52    d_in.bits.b5  //Testigo del interruptor 52
#define Kreg   d_in.bits.b6  //Testigo del interruptor de campo
#define Bkreg  d_out.bits.b0 //Acci�n sobre la bobina del contactor de excitacion
#define Bk2    d_out.bits.b1 //Acci�n sobre la bobina del contactor de arranque por baterias
#define Bi52   d_out.bits.b2 //Acci�n sobre la bobina del interruptor I52

int reg_out;  //Variable Utilizada para resetear DO ante un reset

void control_1(float *in, float *out); //Controlador de Regulaci�n de Voltaje
void v_espac(float *in, float *out);   //Vector espacial de Voltaje
void sin_phi(float *in, float *out);   //Seno del angulo vs e is


void InitPLL_SDRAM(void);
void InitSRU(void);
void SPI_monitor_setup(char *);

float state_sg[22];
float ek_1=0,Uck_1=0,ek_1_2=0,Uck_1_2=0;
bool strb, stpb, scb, svb, arrbat, ref_arr, c_fp, b_rearm, e_lim;

section ("seg_sdram") unsigned int scratch_pad1[10];
section ("seg_sdram") unsigned int scratch_pad2[10];

section ("seg_sdram") float Duty_a[LADQ];
section ("seg_sdram") float Duty_b[LADQ];
section ("seg_sdram") float Duty_c[LADQ];
section ("seg_sdram") float data0[LADQ];//Corriente de Campo
section ("seg_sdram") float data1[LADQ];//Corriente fase T
section ("seg_sdram") float data2[LADQ];//Corriente fase S
section ("seg_sdram") float data3[LADQ];//Corriente fase R
section ("seg_sdram") float data4[LADQ];//Voltaje Bus DC
section ("seg_sdram") float data5[LADQ];//Voltaje T-R
section ("seg_sdram") float data6[LADQ];//Voltaje S-T
section ("seg_sdram") float data7[LADQ];//Voltaje R-S
section ("seg_sdram") float data8[LADQ];//Voltaje R-S
section ("seg_sdram") float ie[LADQ];//Corriente de Campo
section ("seg_sdram") float us[LADQ];//Vector Espacial
section ("seg_sdram") float Ur[LADQ];//Referencia de Tension
section ("seg_sdram") float Urs[LEN];//Voltaje de las L�neas   
section ("seg_sdram") float Ust[LEN];//Voltaje de las L�neas 
section ("seg_sdram") float Utr[LEN];//Voltaje de las L�neas 
section ("seg_sdram") float Ufrs[LEN1];//Voltaje RMS 
section ("seg_sdram") float Urms[LEN];//Voltaje RMS 
section ("seg_sdram") int dataI[LADQ];



void main( void )
{
  union{
  	struct{
  		unsigned :25;
  		unsigned b6:1;
  		unsigned b5:1;
  		unsigned b4:1;
  		unsigned b3:1;
  		unsigned b2:1;
  		unsigned b1:1;
  		unsigned b0:1;
           }bits;
   int mbyte;
        } d_in,d_out;	
  int it,itmp,it1;
  int nbusy,mcnt,version;
  float *cba; //Puntero del buffer circular
  float in[3],in_2[6],vo[1];
  float Da,Db,Dc,K0,Uref=0.0,Ureft=0.0,Urefp=0.0,Usin=0.0;
  float Ulim,Ilim,Uac=0.0,Umed=0.0,Imed=0.0,Smed=0.0,D_c;
  float i_ctrl[2],o_ctrl[1];
  int DA,DB,DC;
  
 
  //****------------------------------------------------------------
  // No modificar esta parte del c�digo
  //****------------------------------------------------------------
  InitPLL_SDRAM();
  InitSRU();
  //
  for(it=0;it<10;it++)
    scratch_pad2[it]=it;
  for(it=0;it<22;it++)
    state_sg[it]=0;
  SPI_monitor_setup("Regul_Vgen_Isen"); // como argumento usar el nombre del proyecto
  set_flag(SET_FLAG5, SET_FLAG); // signals the raspberry program running
  //****------------------------------------------------------------
  //****------------------------------------------------------------
  
  // Incluya c�digo de usuario aqu�
  
  itmp=*((int *)SYSCTL);
  SetIOP1(SYSCTL,(itmp|0x0130000)); // Habilita las lineas de MS2 y MS3
  itmp=(*((int *)EPCTL))&0xFFFFFFE7;
  SetIOP1(EPCTL,itmp|0x20); // Bank3 no sdram core high priority
  itmp=*((int *)AMICTL3);
  *((int *)AMICTL3)=AMIEN|
  BW32|
  WS31|
  HC1|
  IC1|
  AMIFLSH|
  RHC1|
  PREDIS;
  SetIOP1(PROT_BYPASS,0);
  asm("nop;");
  asm("nop;");
  SetIOP1(PWM_RESET,1);
  asm("nop;");
  SetIOP1(PWM_RESET,0);
  asm("nop;");
  SetIOP1(PWM_OFF,00);
  asm("nop;");
  SetIOP1(CLK_SEL,0);
  asm("nop;");
  SetIOP1(PWM_MAX,2000);
  asm("nop;");
  SetIOP1(PWMDT,20);
  SetIOP1(PWMPMIN,20);
  asm("nop;");
  SetIOP1(PWM1_A,0);
  SetIOP1(PWM1_B,0);
  SetIOP1(PWM1_C,0);
  SetIOP1(PWM1_RDY,0);
  asm("nop;");

  SetIOP1(PWM1_EN,1);
 
  itmp=0;
  seed=0;
  version=((GetIOP1(VERSION_H)&0xFFFF)<<16)|((GetIOP1(VERSION_L&0xFFFF)));
  itmp=(GetIOP1(PROT1)&0xFFFF);
 
  asm("nop;"); 
  asm("nop;");
  
  interrupt(SIG_IRQ1, general_isr1);    // enable IRQ1 interrupt
  SetIOP1(AD_ENA,1);
  //
  asm("nop;");

  SetIOP1(PWM_ON,0);
  asm("nop;");

//*************************************  
//Inicializo las Variables 
//*************************************
  d_out.mbyte=0x0; //Salidas Digitales
  SetIOP1(DIG_OUT,d_out.mbyte);
  strb=0;
  stpb=0;
  scb=0;
  svb=0;
  arrbat=0;
  ref_arr=0;
  it=0;
  it1=0;
  Ilim=0.4;  //Limite de corriente
  Ulim=225; //Voltaje Limite
  ek_1=0; //Se�ales del controlador de voltaje
  Uck_1=0;//Se�ales del controlador de voltaje
  Umed=0.0;
  c_fp=0;//Bit de control para el factor de potencia 
  b_rearm=0;//Bit de rearme
  e_lim=0;//Bit de ajuste de los limitadores de corriente y voltaje
  cba=Ufrs; //El valor del puntero es el vector Ufrs
  for(;;)
  {
    do{
   		asm("nop;");
      }
   	while(seed!=2);
   	seed=0;

//***************************************
//LECTURA DE ENTRADAS DIGITALES
//***************************************      	
   	d_in.mbyte=GetIOP1(DIG_IN);
//***************************************
//MANEJO DE SALIDAS DIGITALES
//***************************************
//**************    
//EL ARRANQUE
//**************	
    if ((str==0)&(strb==0)&(arrbat==0))
       {
   	   	Bkreg=1;
   	   	Bk2=1;
   	   	Bi52=0;
   	   	Uref=0.0;
   	   	Usin=0.0;
   	   	ref_arr=1;
   	   	strb=1;     //Activo el bit de arranque
   	   	stpb=0;     //Desactivo el bit de parada
   	   	scb=0;      //Desactivo el bit de sobre corriente
   	   	svb=0;      //Desactivo el bit de sobre voltaje
   	   	c_fp=0; 	//Bit de control para el factor de potencia 
        b_rearm=0;	//Bit de rearme
        e_lim=0;	//Bit de ajuste de los limitadores de corriente y voltaje
   	   	arrbat=1;   //Activo el bit de arranque por baterias
   	   	it=0;
   	   	it1=0;
   	   	Ilim=0.4;  //Limite de corriente
        Ulim=225;  //Voltaje Limite
        ek_1=0;    //Se�ales del controlador
        Uck_1=0;   //Se�ales del controlador
        Umed=0.0;
       }
//************
//LA PARADA
//************
   	if ((stp==0)|(scb==1)|(svb==1))
       {
   	   	Bkreg=0;
   	   	Bk2=0;
   	   	Bi52=0;
   	   	Uref=0.0;
   	   	Usin=0.0;
   	   	ref_arr=0;
   	   	strb=0;     //Desactivo el bit de arranque
   	   	stpb=1;     //Activo el bit de parada
   	   	scb=0;      //Desactivo el bit de sobre corriente
   	   	svb=0;      //Desactivo el bit de sobre voltaje
   	   	c_fp=0;     //Bit de control para el factor de potencia 
        b_rearm=0;  //Bit de rearme
        e_lim=0;    //Bit de ajuste de los limitadores de corriente y voltaje
   	   	arrbat=0;   //Desactivo el bit de arranque por baterias
   	   	it=0;
   	   	it1=0;
   	   	Ilim=0.4;  //Limite de corriente
        Ulim=225;  //Voltaje Limite
        ek_1=0;    //Se�ales del controlador
        Uck_1=0;   //Se�ales del controlador
        Umed=0.0;
       }   
   	SetIOP1(DIG_OUT,d_out.mbyte);
   	it++;  //Incremento de las muestras son 1000 cada 100ms
//********************************
// FIN DE ARRANQUE POR BATER�AS
//********************************  
      if ((arrbat==1)&(Umed>120))
         { 
           arrbat=0;
           Bk2=0;
         }      
//***************************************
//MANEJO DE LA REFERENCIA EN EL ARRANQUE
//***************************************     	
      if ((it>=LEN)&(ref_arr==1))
       { 
        Uref=Uref+1.0;
        if ((Uref>207.0)|(Umed>207.0))
            ref_arr=0;
       }         	
//***************************************
//SUBIR O BAJAR LA REFERENCIA 
//***************************************  
//
// Con el Interruptor del Generador I52 Abierto
//  
    if (I52==1)
       {
        if ((it>=LEN)&(up==0))
           Uref=Uref+1.0;
        if ((it>=LEN)&(dwn==0))
           Uref=Uref-1.0; 
        //  
        //Reseteo las se�ales de referencia y control del
        //corrector del factor de potencia
        //   
        Usin=0.0;   
       }
//
// Con el Interruptor del Generador I52 Cerrado
//  
    if (I52==0)
       {
        if ((it>=LEN)&(up==0))
           Usin=Usin+0.05;
        if ((it>=LEN)&(dwn==0))
           Usin=Usin-0.05; 
       }

//***************************************
//LIMITADOR DE CORRIENTE 
//***************************************       
    if ((it>=LEN)&(e_lim==1)&(up==0))
       Ilim=Ilim+0.002;       
    if ((it>=LEN)&(e_lim==1)&(dwn==0))
       Ilim=Ilim-0.002;        
//***************************************
//LIMITACION DE CORRIENTE Y SOBRE VOLTAJE
//*************************************** 
//*********************************************      
//Con el Interruptor del Generador I52 Abierto
//*********************************************
    if (((Imed>=Ilim)|(Umed>=Ulim))&(it>=LEN)&(I52==1))
       Uref=Uref-1.5; 
//*********************************************      
//Con el Interruptor del Generador I52 Cerrado
//*********************************************       
    if (((Imed>=Ilim)|(Umed>=Ulim))&(it>=LEN)&(I52==0))
       Usin=Usin-0.005;
//
//Referencia Totalizada
//       
    Ureft=Uref+Usin;
//       
    Ur[it]=Ureft; 
//*****************************************
//SOBRE CORRIENTE Y SOBRE VOLTAJE EXTREMO
//*****************************************       
    if (Imed>0.7)
       scb=1;
    if (Umed>240)
       svb=1; 
//******************************************
//MANEJO DEL INTERRUPTOR DEL GENERADOR I52
//******************************************
//******************************************
//Manejo del control del I52
//******************************************
//******************************************
//Para el cierre del interruptor I52
//******************************************
    if ((it>=LEN)&(strb==1)&(ref_arr==0)&(b_rearm==0)&(I52==1)&(c_i52==0))
       {
       	it1++;
        if (it1>=5)
           {
            Bi52=1;    //Ordeno cierre del interruptor del generador
            b_rearm=1; //Bit de rearmada
            it1=0;
           }     
       }
    if ((strb==1)&(ref_arr==0)&(I52==1)&(c_i52==1))
       {
        it1=0;
       }
	if ((b_rearm==1)&(I52==0)&(c_i52==1))  //Rearmada del sistema
       b_rearm=0;  
       
//******************************************
//Para la apertura del interruptor I52
//******************************************    
    if ((it>=LEN)&(strb==1)&(ref_arr==0)&(b_rearm==0)&(I52==0)&(c_i52==0))
       {
       	it1++;
        if (it1>=5)
           {
            Bi52=0;    //Ordeno apertura del interruptor del generador
            b_rearm=1; //Bit de rearmada
            it1=0;
           }           
       }
    if ((strb==1)&(ref_arr==0)&(b_rearm==0)&(I52==0)&(c_i52==1))
       {
        it1=0;
       }
    if ((b_rearm==1)&(I52==1)&(c_i52==1))   //Rearmada del sistema
        b_rearm=0;   
//***************************************
//MANEJO DE LOS DATOS
//***************************************       
   	if (it>=LEN)  // Mil muestras cada 100 mseg aproxim
          it=0;
    data0[it]=(float)ch0*Ga0; //Corriente de Campo
    data1[it]=(float)ch1*Ga1; //Corriente fase T
   	data2[it]=(float)ch2*Ga2; //Corriente fase S
   	data3[it]=(float)ch3*Ga3; //Corriente fase R
   	data4[it]=(float)ch4*Ga4; //Voltaje Bus DC
   	data5[it]=(float)ch5*Ga5; //Voltaje T-R
   	data6[it]=(float)ch6*Ga6; //Voltaje S-T
   	data7[it]=(float)ch7*Ga7; //Voltaje R-S
   	dataI[it]=version;
  
//***************************************
//MEDIDA DE LA CORRIENTE DE CAMPO
//***************************************  
    Imed=(float)ch0*Ga0; 
    ie[it]=Imed;
//**********************************************
//MEDIDA DE LOS VOLTAJES DE LINEAS Y VALOR RMS
//**********************************************    
      Urs[it]=(float)ch7*Ga7;
      Ust[it]=(float)ch6*Ga6;
      Utr[it]=(float)ch5*Ga5;
      cba[0]=Urs[it];// 167 muestras cada 16,7mseg aproxim
      Urms[it]=rmsf(Ufrs, LEN1);// Valor RMS
      cba=circptr(cba,1,Ufrs,LEN1);//Buffer Circular
//****************************************  
// Vector Espacial de Voltajes - Magnitud
//**************************************** 
     in[0]=Urs[it];
     in[1]=Ust[it];
     in[2]=Utr[it];
     v_espac(in,vo);
     us[it]=vo[0];
     Umed=us[it]/SQRT2;
//***********************************************************************  
// Medida ISEN entre los Vectores Espaciales de Voltaje y Corriente
// esta se�al se le adiciona a la medida de voltaje Umed
//*********************************************************************** 
     in_2[0]=Urs[it];
     in_2[1]=Ust[it];
     in_2[2]=Utr[it];
     in_2[3]=data3[it]; //Corriente fase R
     in_2[4]=data2[it]; //Corriente fase S
     in_2[5]=data1[it]; //Corriente fase T
     sin_phi(in_2,vo);
     Smed=vo[0];
     if (I52==0)
         Umed=Umed+Smed;
      
//********************************+**
// EL CONTROLADOR DE VOLTAJE DE LINEA
//***********************************
      i_ctrl[0]=Ureft;
      i_ctrl[1]=Umed;
      control_1(i_ctrl,o_ctrl);
      D_c=o_ctrl[0];
      data8[it]=D_c;
          
//***************************************
//MANEJO DEL PWM
//***************************************   	
   	SetIOP1(PWM1_A,D_c);
   	SetIOP1(PWM1_B,D_c);
   	SetIOP1(PWM1_RDY,0);      
//******************************************
 }   
  asm("nop;");
  asm("nop;");
  SetIOP1(AD_ENA,0);
  SetIOP1(PWM1_EN,0);
  SetIOP1(PWM_OFF,00);				// disable PWM outputs 
  asm("nop;");
  interrupt(SIG_IRQ0, SIG_IGN);    // disable IRQ0 interrupt
  interrupt(SIG_IRQ1, SIG_IGN);    // disable IRQ1 interrupt
  set_flag(SET_FLAG5, CLR_FLAG); // signals the raspberry program not running
  //****------------------------------------------------------------
  // No modificar esta parte del c�digo
  //****------------------------------------------------------------   
  //Generating Code for connecting : FLAG6 to DPI_PIN7 (LED2)
  SRU (HIGH, DPI_PBEN07_I);
  SRU (FLAG6_O, DPI_PB07_I);
  asm volatile("bit set flags FLG6O;");
  asm volatile("nop;");	
  asm volatile("bit clr flags FLG6;");
  for(;;)
   {
   	 for(it=0;it<1000000;it++)
   	   asm("nop;");
   	 asm volatile("bit tgl flags FLG6;");
   }
}

void v_espac(float *Vin, float *Vout)
{
 float Va,Vb,Vc,us_x,us_y;;
 float r=2.0943951023931954923084289221863; //dos pi tercios
 float r3=1.7320508075688772935274463415059;
 
 Va=Vin[0];
 Vb=Vin[1];
 Vc=Vin[2];
 us_x=2.0/3.0*(Va+Vb*cosf(r)+Vc*cosf(2*r));
 us_y=2.0/3.0*(Vb*sinf(r)+Vc*sinf(2*r));
 Vout[0]=sqrt(us_x*us_x+us_y*us_y);
}

void sin_phi(float *Vin, float *Vout)
{
 float Va,Vb,Vc,Ia,Ib,Ic,us_x,us_y,is_x,is_y;
 float u_s,u_i,i_s,sin_d,sinphi;
 float r=2.0943951023931954923084289221863; //2pi/3
// float l=0.52359877559829887307710723054658; pi/6
 float l=0.0;
 float r3=1.7320508075688772935274463415059;
 
 Va=Vin[0];
 Vb=Vin[1];
 Vc=Vin[2];
 Ia=Vin[3];
 Ib=Vin[4];
 Ic=Vin[5];
 us_x=(Va*cosf(-l)+Vb*cosf(r-l)+Vc*cosf(2*r-l));
 us_y=(Va*sinf(-l)+Vb*sinf(r-l)+Vc*sinf(2*r-l));
 is_x=(Ia+Ib*cosf(r)+Ic*cosf(2*r));
 is_y=(Ib*sinf(r)+Ic*sinf(2*r));
 u_s=sqrt(us_x*us_x+us_y*us_y);
 i_s=sqrt(is_x*is_x+is_y*is_y);
 u_i=us_x*is_y-us_y*is_x;
 sin_d=u_s*i_s;
 if (sin_d>0.000001)
    sinphi=u_i/sin_d;
 else
    sinphi=0.0;   
 if (sinphi>=1.0)
    sinphi=1.0; 
 if (sinphi<=-1.0)
    sinphi=-1.0;          
 Vout[0]=0.5*i_s*sinphi;

}

void control_1(float *sg_in, float*vcont)
{
 float ek,ref,med,Uck;
 
 ref=sg_in[0];
 med=sg_in[1];
 ek=ref-med;
 Uck=Uck_1+3.5*ek-0.55*ek_1;
 if (Uck>1900)
    Uck=1900;
 if (Uck<100)
    Uck=100;   
 vcont[0]=Uck;
 ek_1=ek;
 Uck_1=Uck;
}
