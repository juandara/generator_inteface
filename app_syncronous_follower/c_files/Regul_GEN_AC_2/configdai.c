#include <sru.h>
#include <def21369.h>

// This function will setup the SRU Registers
void InitSRU(void)
{
	// Enable pull-up resistors on unused DAI pins
	* (volatile int *)DAI_PIN_PULLUP = 0xfffff;

	// Enable pull-up resistors on unused DPI pins
	* (volatile int *)DPI_PIN_PULLUP = 0x27d0;

	//Generating Code for connecting : DPI_PIN1 to SPI_MOSI
	SRU (LOW, DPI_PBEN01_I); 
	SRU (DPI_PB01_O, SPI_MOSI_I); 

	//Generating Code for connecting : DPI_PIN3 to SPI_CLK
	SRU (LOW, DPI_PBEN03_I); 
	SRU (DPI_PB03_O, SPI_CLK_I); 

	//Generating Code for connecting : DPI_PIN4 to SPI_DS
	SRU (LOW, DPI_PBEN04_I); 
	SRU (DPI_PB04_O, SPI_DS_I); 

	//Generating Code for connecting : FLAG4 to DPI_PIN12
	SRU (HIGH, DPI_PBEN12_I); 
	SRU (FLAG4_O, DPI_PB12_I); 

	//Generating Code for connecting : FLAG5 to DPI_PIN13
	SRU (HIGH, DPI_PBEN13_I); 
	SRU (FLAG5_O, DPI_PB13_I); 

	//Generating Code for connecting : FLAG7 to DPI_PIN6
	SRU (HIGH, DPI_PBEN06_I); 
	SRU (FLAG7_O, DPI_PB06_I); 

	//Generating Code for connecting : SPI_MISO to DPI_PIN2
	SRU (HIGH, DPI_PBEN02_I); 
	SRU (SPI_MISO_O, DPI_PB02_I); 


}
