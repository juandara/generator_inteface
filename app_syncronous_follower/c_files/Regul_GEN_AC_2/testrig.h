#define  SetIOP(addr, val)  (* (volatile float *) addr) = (val)
#define  GetIOP(addr)       (* (volatile float *) addr)

							
#define  SetIOP1(addr, val)  (* (volatile int *) addr) = (val)
#define  GetIOP1(addr)       (* (volatile int *) addr)

#define FPGA_BASE		0x0C000000
#define PWM_RESET		0x0C000000
#define PWM_OFF			0x0C000001
#define PWM_ON			0x0C000002
#define	VERSION_L		0x0C000003
#define	VERSION_H		0x0C000004
#define	PWMDT			0x0C000005
#define	PWMPMIN			0x0C000006
#define	PWM_MAX			0x0C000007
#define	TFLAG			0x0C000008
#define	PROT1			0x0C000009
#define	PROT2			0x0C00000A
#define	AD_ENA			0x0C00000B
#define	PWM1_EN			0x0C00000C
#define	PWM2_EN			0x0C00000D
#define	PWM3_EN			0x0C00000E
#define PWM1_A			0x0C000012
#define PWM1_B			0x0C000011
#define PWM1_C			0x0C000010
#define PWM2_A			0x0C000013
#define PWM2_B			0x0C000014
#define PWM2_C			0x0C000015
#define PWM3_A			0x0C000016
#define PWM3_B			0x0C000017
#define PWM3_C			0x0C000018
#define	PWM1_RDY		0x0C00001A
#define	PWM2_RDY		0x0C00001B	
#define	PWM3_RDY		0x0C00001C
//--------------------------------------------------------------
		// bit en 1 desahabilita IGBTs
		// seis bits para controlar 6 IGBTS
		// HC LC HB LB HA LA
//--------------------------------------------------------------		
#define	CLK_SEL			0x0C000030
#define	AD_CTL			0x0C000035

#define	CNT_MAX			0x0C000040
#define	CNT_Z			0x0C000041
#define	DIR				0x0C000042
#define	SPEED_L			0x0C000143
#define	SPEED_H			0x0C000144
#define	POS_REG			0x0C000145

#define	REG_TMP			0x0C0000AA
#define	DIG_OUT			0x0C0000F0
#define	DIG_IN 			0x0C0000F1

#define	AD_CH0			0x0C000100
#define	AD_CH1			0x0C000101
#define	AD_CH2			0x0C000102
#define	AD_CH3			0x0C000103
#define	AD_CH4			0x0C000104
#define	AD_CH5			0x0C000105
#define	AD_CH6			0x0C000106
#define	AD_CH7			0x0C000107

#define	OFF0			-58
#define	OFF1			-44
#define	OFF2			-124
#define	OFF3			-119
#define	OFF4			-74
#define	OFF5			-68
#define	OFF6			109
#define	OFF7			41
#define	Ga0				1.0e-03
#define	Ga1				9.89e-04
#define	Ga2				9.89e-04
#define	Ga3				9.9e-04
#define	Ga4				0.0203486
#define	Ga5				0.0204148
#define	Ga6				0.02036511
#define	Ga7				0.02039821

#define	PROT_BYPASS		0x0C000200

int seed;
unsigned int speed_reg,pos;
int ch0,ch1,ch2,ch3,ch4,ch5,ch6,ch7;

//IRQ0 interrupt service routine
void general_isr0 (int sig)
{
 unsigned int itmp;
 
 itmp=GetIOP1(SPEED_H)&0xFFFF;
 speed_reg=(itmp<<16)|(GetIOP1(SPEED_L)&0xFFFF);
 pos=GetIOP1(POS_REG)&0xFFFF;
}

//IRQ1 interrupt service routine
void general_isr1 (int sig)
{
 unsigned int tmp;
 float ftmp;
 
 seed=2;
 tmp=GetIOP1(AD_CH0)&0xFFFF;
 ch0=(tmp>0x7FFF)?tmp-0x10000:tmp;
 ch0-=OFF0;
 
 tmp=GetIOP1(AD_CH1)&0xFFFF;
 ch1=(tmp>0x7FFF)?tmp-0x10000:tmp;
 ch1-=OFF1;
 
 tmp=GetIOP1(AD_CH2)&0xFFFF;
 ch2=(tmp>0x7FFF)?tmp-0x10000:tmp;
 ch2-=OFF2;
 
 tmp=GetIOP1(AD_CH3)&0xFFFF;
 ch3=(tmp>0x7FFF)?tmp-0x10000:tmp;
 ch3-=OFF3;
 
 tmp=GetIOP1(AD_CH4)&0xFFFF;
 ch4=(tmp>0x7FFF)?tmp-0x10000:tmp;
 ch4-=OFF4;
 
 tmp=GetIOP1(AD_CH5)&0xFFFF;
 ch5=(tmp>0x7FFF)?tmp-0x10000:tmp;
 ch5-=OFF5;
 
 tmp=GetIOP1(AD_CH6)&0xFFFF;
 ch6=(tmp>0x7FFF)?tmp-0x10000:tmp;
 ch6-=OFF6;
 
 tmp=GetIOP1(AD_CH7)&0xFFFF;
 ch7=(tmp>0x7FFF)?tmp-0x10000:tmp;
 ch7-=OFF7;
}
